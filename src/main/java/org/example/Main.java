package org.example;

import java.util.Map;

public class Main {
    public static void main(String[] args) {
        //Создания объекта translator для работы с классом Translator
        Translator translator = new Translator();
        //Наполняем словарь через метод для наполнения Мапы
        translator.putNewWord("Car", "Машина");
        translator.putNewWord("Airplane", "Самолёт");
        translator.putNewWord("Bike", "Велосипед");
        translator.putNewWord("Scooter", "Самокат");
        translator.putNewWord("Motorcycle", "Мотоцикл");
        translator.putNewWord("Wing", "Крыло");
        translator.putNewWord("Butterfly", "Бабочка");
        translator.putNewWord("Bird", "Птица");

        //Перебираем все элементы Мапы и выводим на экран
        translator.showEntireDictionary(translator.englishRussianTranslator);

        //Вывести перевод запрашиваемого слова
        translator.getTranslation("Wing");

        //Удаляем из словаря слово с переводом
        translator.removeEnglishRussianTranslator("Bird");

        //Выводим на экран словарь после удаления слова
        translator.showEntireDictionary(translator.englishRussianTranslator);
    }
}
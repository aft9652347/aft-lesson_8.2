package org.example;

import java.util.HashMap;
import java.util.Map;

public class Translator {
    //Создаём мапу для хранения англо-русского словаря
    Map<String, String> englishRussianTranslator = new HashMap<>();
    //метод для добавлять в базу новых слов с переводом
    public void putNewWord (String word, String translation){
        englishRussianTranslator.put(word, translation);
    }
    //Метод для вывода на экран всего словаря (переборы мапы)
    public void showEntireDictionary(Map<String,String> dictionaryForOutput) {
        for (Map.Entry<String, String> entry : dictionaryForOutput.entrySet()) {
            System.out.println(String.format("Английское слово: %s,  русский перевод: %s"
                    , entry.getKey(), entry.getValue()));
        }
    }

    //Метод для получения русского перевода для переданного английского слова
    public void getTranslation(String englishWord) {
        System.out.println(String.format("Перевод для слова %s: %s", englishWord, englishRussianTranslator.get(englishWord)));
    }
    //Метод для удаления из базы слова с переводом.
    public void removeEnglishRussianTranslator(String removeEnglishWord){
        englishRussianTranslator.remove(removeEnglishWord);
    }

}
